1. What is Plasma Next?
2. Using Plasma Next
3. Design
4. Develop
5. Foundations
    1. Accessibility
        1. Overview
        2. Principles for accessible design
    2. Accessibility Basics
        1. Assistive technology
        2. Layout and typography
        3. Writing
        4. Implementing accessibility
    3. Patterns
        1. Color and contrast
            1. Contrast ratios
            2. Clustering elements
        2. Text resizing
            1. Background
            2. Methods
            3. Increase container size
                1. Reflow the layout
                2. Enable content to scroll
                3. User long press tooltips to provide enlarged labels
        3. Design to Implementation
            1. Designing and implementing accessing UX
            2. Overview
            3. Structure: Web Landmarks and headings
                1. Define Landmarks
                    1. Add accessible labels
                    2. Define headings
                    3. Consider hierarchy in addition to style
                2. Flow: Focus order and key traversal
                    1. Define and optimize the linear and keyboard-assisted user experience
                    2. Group product use cases
                    3. Define initial focus and component-level focus
                    4. Define any atypical key traversla through the page and components
                    5. Elements
                        1. Add accessibility labels, define roles, and indicate decorative elements
                        2. Label elements
                        3. Add labels for meaningful images and interactive elements
    4. Assign a role to interactive elements
        1. Content Design
            1. Resources
            2. What's new
            3. Alt text
                1. How to write great alt text
                    1. Overview
                    2. Deciding when tio use alt text
                    3. Focus on the meaning of the image
                    4. Keep it short
                    5. Dont start alt text with "image of"
                2. Context and nearby text
                    1. Context matters
                    2. Alt text is subjective
                    3. Caoptions should benefit all users
                3. Types of imagery
                    1. When to describe the type of image
                    2. Charts and graphs
                    3. Video and motion alt text
            4. Global writing
                1. Overview
                2. Global writing
                3. Word Choice
                    1. Use global examples and explain local references\
                    2. Use short, simple sentences
                    3. Avoid abbreviations
                    4. Clarify pronouns
                    5. Clarify "this" and "that"
                    6. Avoid idiomatic, colloquial, and polite expressions
                    7. Reduce technical jargon
                    8. Clarify ambiguities
            5. Style guide
                1. UX writing best practices
                    1. Explain consequences
                    2. Use scannable words and formats
                    3. Use sentence case
                    4. Use abbreviations sparingly
                2. Word choice
                    1. Pronouns
                    2. Use second person pronouns ("you")
                    3. Don't combine first and second person
                    4. Use caution with "I" and "we"
                3. Grammar and punctuation
                    1. Skip periods and unnecessary punctuation
                    2. Use contractions
                    3. Use serial commas
                    4. Skip colons in headings
                    5. Use explamation points sparingly
                    6. Use ellipses sparingly
                    7. Use parentheses to define terms
                    8. Skip ampresands in body text
                    9. Use dashes with caution
                    10. Use hyphens with care
    5. Customizing Plasma Next
        1. Dynamic color makes personal devices feel personal
            1. Applying a brand color system
        2. Get started
            1. Set-up tutorials
            2. Set-up
            3. Dynamic color tutorial
            4. Material theme builder
        3. Custom color schemes
            1. Color roles
    6. Design Tokens
        1. Overview
            1. Resources
            2. Takeaways
            3. What’s a design token?
            4. Why are tokens important?
            5. Tokens and Material Design
            6. Deciding if tokens are right for you
        2. How to read tokens
            1. Token tables
                1. Styles
                2. Component specs
                3. Using token tables
            2. Reading token names
            3. Types of tokens
                1. Reference tokens
                2. System tokens
                3. Component tokens
            4. Contexts: Different default values
        3. How to use tokens
            1. Download baseline tokens
            2. Use tokens in Figma
                1. Generate tokens
                2. Import tokens via DSP
                3. Update token values
                    1. Using the Material Theme Builder Figma plugin (update colors only)
                    2. Using Figma styles
                    3. Use tokens in product mock-ups
                    4. Use tokens with the Material Design Kit
                    5. Export tokens
    7. Interaction
        1. Gestures
            1. Resources
            2. Types of gestures
                1. Tap
                2. Double tap
                3. Long press
                4. Scroll and Pan
                5. Swipe
                6. Drag
                7. Pick up and move
                8. Pinch
                9. Compound gestures
        2. Inputs
            1. External inputs for devices
                1. Common features of external inputs
                    1. Mouse
                    2. Trackpad
                    3. Physical keyboard
                2. Input device behaviors
            2. Mouse and cursor interactions
                1. Primary click
                    1. Move cursor
                2. Secondary click
                    1. Context menus
                3. Hover
                    1. Image hover
                    2. Text hover
                    3. Non-editable text
                4. Text selection
                5. Text selection with touch control
            3. Mouse wheel and trackpad gestures
                1. Vertical scroll
                2. Touch scroll and mouse text selection
                3. Horizontal scroll
            4. Physical keyboard
                1. Show and hide virtual keyboard
                2. Common keyboard interactions
                    1. Enter key
                    2. Spacebar control
        3. Selection
            1. Resources
            2. Selection indicators
            3. Types of selection
                1. Touch
                2. Entering and exiting selection mode
                3. Large selections
                4. Click
        4. States
            1. Overview
                1. Resources
                    1. Enabled
                    2. Disabled
                    3. Hover
                    4. Focused
                    5. Activated
                    6. Pressed
                    7. Dragged
            2. State layers
                1. “On” colors
                2. State layers tokens and values
                3. Activated state
            3. Applying states
                1. Enabled
                2. Disabled
                    1. Behavior
                3. Hover
                    1. Behavior
                4. Focused
                    1. Behavior
                5. Activated
                    1. Behavior
                6. Pressed
                    1. Behavior
                7. Dragged
                    1. Behavior
    8. Layout
        1. Understanding layout
            1. Overview
            2. Differences from M2
            3. Layout terms
            4. Spacing
                1. Grouping
                2. Margins
                3. Spacers
                4. Padding
                5. Pixel density
                    1. Density-independent pixels
                6. Information density
                    1. Density scale
            5. Parts of layout
                1. Navigation region
                2. Body region
                3. Panes
                    1. Layouts
                    2. App bars
                    3. Columns
            6. Hardware considerations
                1. Display cutout
                2. Foldable devices
                    1. Fold
                    2. Device state
                        1. Folded
                        2. Open flat
                        3. Table top
                    3. Interaction
                        1. App continuity
                        2. Scrolling and multiple pages
        2. Applying layout
            1. Window size classes
                1. Designing across window classes
                    1. What should be revealed?
                    2. How should the screen be divided?
                    3. What should be resized?
                    4. What should be repositioned?
                    5. What should be swapped?
                        1. Common swappable components
            2. Compact
                1. Navigation
                2. Body pane
                3. Spacing
                4. Special considerations
            3. Medium
                1. Navigation
                2. Body pane
                3. Spacing
                4. Special considerations
                    1. Reachability
            4. Expanded
                1. Navigation
                2. Body pane
                3. Spacing
                4. Special considerations
            5. Multi-window
                1. User needs
                2. Window creation and behavior
                    1. Taskbar
                    2. Context menu
                    3. Adjusting window sizes
        3. Canonical layouts
            1. Overview
                1. Resources
                2. Takeaways
                3. Layouts
                    1. Feed
                    2. List-detail
                    3. Supporting pane
            2. List-detail
                1. Usage
                2. Dividing space
                3. Across window size classes
                    1. Compact
                    2. Medium
                    3. Expanded
                4. Behavior
                    1. Single vs two-pane
                    2. Transition between layouts
                    3. From single to two-pane layout
                    4. From two-pane to single-panel layout
            3. Supporting pane
                1. Usage
                2. Dividing space
                3. Across window size classes
                    1. Compact
                    2. Medium
                    3. Expanded
            4. Feed
                1. Usage
                2. Dividing space
                3. Across window size classes
                    1. Compact
                    2. Medium
                    3. Expanded
                4. Behavior
6. Plasma Next Key Terms
    1. Banner
    2. Button
    3. Button app bar
    4. Bottom sheet
    5. Card
    6. Checkbox
    7. Chip
    8. Color: Baseline scheme
    9. Color: Dynamic color
    10. Color: Extended color
    11. Color: Key color
    12. Color: Scheme
    13. Color: Source color
    14. Color: Tonal palette
    15. Color: Tone
    16. Color: User-generated themes
    17. Contrast
    18. Customization
    19. Dark theme
    20. Data table
    21. Date picker
    22. Design attribute
    23. Design guidelines
    24. Design specs
    25. Design System Package (DSP)
    26. Design tokens
    27. Design tokens: Context
    28. Design tokens: Role
    29. Design tokens: Type
    30. Design tokens: Value
    31. Dialog
    32. Divider
    33. Element
    34. Extended FAB
    35. Floating action button (FAB)
    36. HCT
    37. Image list
    38. Libraries
    39. List
    40. Material Components
    41. Material Design
    42. Material Theming
    43. Menu
    44. Mode
    45. Navigation bar
    46. Navigation drawer
    47. Navigation rail
    48. Progress indicator
    49. Radio button
    50. Role
    51. Side sheet
    52. Slider
    53. Snackbar
    54. Style
    55. Switch
    56. Tab
    57. Text field
    58. Theme
    59. Time picker
    60. Tooltip
    61. Top app bar
7. Styles
    1. Color
        1. Color system
            1. Overview
                1. Resources
                2. What’s new
                    1. Reorganized guidelines
                    2. Tone-based surface color
                    3. Additional accent colors
            2. How the system works
                1. It’s like paint-by-number
                2. Essential terms
                3. Dynamic color generates color schemes
                    1. It starts with a source color
                        1. Generate it from a wallpaper
                        2. Generate it from in-app content
                        3. Pick it by hand
                    2. The source color is fed into an algorithm
                    3. The algorithm generates key colors
                    4. It creates tonal palettes
                    5. It assists tones to color roles
                    6. Color roles feed into UI elements
                4. Supporting three levels of contrast
                5. Pairing accessible tones
                6. Defining color with hue, chroma, and tone (HCT)
                    1. Hue
                    2. Chroma
                    3. Tone
        2. Color roles
            1. What are color roles?
            2. General concepts
                1. Pairing and layering colors
            3. Primary
            4. Secondary
            5. Tertiary
            6. Error
            7. Surface
                1. Inverse colors
                2. Deprecated - Surface tint overlays
            8. Outline
            9. Add-on color roles
                1. Fixed accent colors
                2. On fixed accent colors
                3. Bright and dim surface roles
        3. Color schemes
            1. Choosing a scheme
                1. Static color
                    1. Use static (baseline) color if
                2. Dynamic color
                    1. Use dynamic color if
            2. Static
                1. Baseline
                    1. Baseline colors
                    2. Baseline color tokens
                    3. Design with baseline
                        1. Use the Design Kit and M3 baseline colors in new design files
                        2. Apply baseline colors to an existing file
                2. Custom brand
                    1. Create a custom brand color scheme
                    2. Design with brand colors
                        1. Use brand colors in new design files
                        2. Apply brand colors to an existing file or M3 Design Kit components
                    3. Develop with brand colors
            3. Dynamic
                1. Choosing a source
                2. User-generated source
                3. Content-based source
        4. Advanced
            1. Overview
            2. Apply colors
                1. Combine multiple color schemes
                    1. Why
                    2. How
                    3. Best practices
                2. Map or remap colors on UI elements
                    1. Why
                    2. How
                    3. Design
                    4. Best practices
                3. Apply dynamic color to images
                    1. Why
                    2. How
                        1. Vector assets
            3. Define new colors
                1. Define static colors
                    1. How
                    2. Why
                    3. Best practices
                2. Define custom color rules
                    1. Why
                    2. How
                    3. Best practices
            4. Adjust existing colors
                1. Define your own baseline schemes
                    1. Why
                    2. How
                    3. Best practices
                2. Define your own dynamic scheme
                    1. Why
                    2. How
                    3. Best practices
                3. Use color fidelity
                    1. Why
                    2. How
                    3. Best practices
                4. Harmonize colors
                    1. Why
                    2. How
                    3. Best practices
        5. Color resources
            1. Material theme builder
            2. Tutorials
    2. Elevation
        1. Overview
            1. All surfaces and components have elevation values
                1. Resting
                2. Changing
        2. Applying elevation
            1. Depicting elevation
                1. Tonal difference
                2. Surface color roles and elevation
            2. Shadows
                1. When to use visible shadows
                    1. Product elements
                    2. Encourage interaction
                2. Scrims
        3. Tokens
            1. Elevation tokens
            2. Component elevation
    3. Icons
        1. Overview
            1. Resources
            2. What’s new
        2. Designing icons
            1. Designing principles
            2. Icon size and layout
                1. Additional optical icon sizes
                2. Standard (Baseline) icon layout
            3. Grid and keyline shapes
                1. Icon design template
                2. Icon grid and keyline
            4. Icon metrics
                1. Anatomy
                2. Corners
                3. Weight and stroke
                4. Complex icon shapes
        3. Applying icons
            1. Icon and symbol styles
                1. Outlined style
                    1. Rounded and sharp styles
                2. Customizing symbols
                    1. Weight
                    2. Fill
                    3. Grade
                    4. Optical sizes
                3. Using symbols with typography
                4. Accessibility
                    1. Icons with a label text
                    2. Target size
    4. Motion
        1. Overview
            1. Resources
            2. What’s new
            3. Transition guidance
        2. Easing and duration
            1. Suggested easing and duration pairs
            2. Easing
                1. Choosing an easing set
                2. Choosing an easing type
                3. Begin and end on screen
                4. Enter the screen
                5. Exit the screen permanently
                6. Exit the screen temporarily
            3. Duration
                1. Choosing a duration
                2. Transition size
                3. Enter vs. exit transitions
        3. Transitions
            1. Tokens
            2. Easing
                1. Emphasized easing set
                2. Standard easing set
                3. Duration
                    1. Short durations
                    2. Medium durations
                    3. Long durations
                    4. Extra long durations
                4. Transition Patterns
                    1. Container transform
                    2. Between full-screen views
                    3. Within a screen
                5. Forward and backward
                6. Lateral
                7. Top level
                8. Enter and exit
                    1. Within screen bounds
                    2. Beyond screen bounds
                9. Skeleton loaders
            3. Applying transitions
                1. What makes a good transition?
                2. Follows accessibility settings
                3. Consistent
                4. Stable layouts
                5. No jarring jump cuts
                6. Coherent spatial model
                7. Unified direction
                8. Clean fades
                9. Simple style
            4. Choosing a transition pattern
                1. Container transform
                2. Forward and backward
                3. Lateral
                4. Top level
                5. Enter and exit
    5. Shape
        1. Overview
            1. Differences from M2
        2. Scale and tokens
            1. Shape scale
                1. Baseline shapes
                2. Symmetry
                3. Shape faily
            2. Customizing shapes
                1. For a style
                2. For a specific moment
            3. Shape tokens
    6. Typography
        1. Overview
            1. Resources
            2. Highlights
                1. Variable fonts
                2. Five type styles
                3. Typography tokens
        2. Fonts
            1. Default types
        3. Type scale and tokens
            1. Type scale
            2. Typography tokens
            3. Customizing your type scale
                1. Font size units
                2. Example converstions
                3. Letter spacing units
                4. Letter spacing examples
            4. Adjustable axes
                1. Weight
                2. Grade
                3. Width
                4. Optical size
        4. Applying type
            1. Applying type
            2. Roles
                1. Display
                2. Headline
                3. Title
                4. Body
                5. Label
            3. Typesetting
                1. Using padding and bounding boxes
                2. Using the baseline
            4. Ensuring readability
                1. Line height
                2. Tabular numbers
            5. Using symbols with typography
            6. Accessibility
                1. Color and contrast
8. Components
    1. Overview
        1. Actions
        2. Communication
        3. Containment
        4. Navigation
        5. Selection
        6. Text inputs
    2. App bars
        1. Bottom app bar
            1. Overview
                1. Resources
                2. Differences from Breeze
            2. Specs
                1. Color
                2. Measurements
                3. Configurations
            3. Guidelines
                1. Usage
                2. Anatomy
                    1. Container
                    2. FAB (optional)
                    3. Icon buttons
                3. Placement
                    1. With a top app bar
                    2. With snackbars
                4. Responsive layout
                    1. Window size classes
                        1. Compact and medium
                        2. Expanded
                        3. Density
                        4. Alignment
                    2. Behavior
                        1. Scrolling
                        2. Transitions
                        3. Opening menus
                        4. Elements that cover the bottom
            4. Accessibility
                1. Use cases
                2. Interaction style
                    1. Touch
                    2. Cursor
                    3. Initial focus
                3. Labeling elements
        2. Top app bar
            1. Overview
                1. 
            2. Specs
                1. **Center-aligned top app bar**
                    1. **Center-aligned top app bar color**
                    2. **Center-aligned top app bar measurements**
                2. **Small top app bar**
                    1. **Small top app bar color**
                    2. **Small top app bar measurements**
                3. **Medium top app bar**
                    1. **Medium top app bar color**
                    2. **Medium top app bar measurements**
                4. **Large top app bar**
                    1. **Large top app bar color**
                    2. **Large top app bar measurements**
            3. Guidelines
                1. **Usage**
                2. **Anatomy**
                    1. **Container**
                    2. **Navigation icon**
                    3. **Headline**
                    4. **Trailing interactive icons**
                3. **Responsive layout**
                    1. **Compact**
                    2. Medium
                    3. Expanded
                4. **Behavior**
                    1. Scrolling
                    2. **Nesting actions**
                    3. **Responsiveness**
            4. Accessibility
                1. **Use cases**
                2. **Interaction & style**
                    1. Touch
                    2. Cursor
                    3. Keyboard/switch
                    4. **Initial focus**
                    5. **Keyboard navigation**
                    6. **Labeling elements**
    3. Badges
        1. Overview
            1. **Resources**
            2. Differences
        2. Specs
            1. **Color**
            2. **Measurements**
            3. **Configuration**
        3. Guidelines
            1. **Usage**
                1. **With other components**
            2. **Anatomy**
                1. **Container**
                2. **Label text**
            3. **Placement**
        4. Accessibility
            1. Use cases
            2. Interaction style
            3. **Visual indicators**
            4. **Labeling elements**
    4. Buttons
        1. All buttons
            1. **Choosing buttons**
            2. **Hierarchy**
                1. **Primary action button**
                2. **Other buttons**
            3. **Placement**
        2. Common buttons
            1. Overview
                1. **Resources**
                2. Differences from Breeze
            2. Specs
                1. **Elevated button**
                    1. **Elevated button color**
                    2. **Elevated button states**
                    3. **Elevated button measurements**
                2. **Filled button**
                    1. **Filled button color**
                    2. **Filled button states**
                    3. **Filled button measurements**
                3. **Filled tonal button**
                    1. **Filled tonal button color**
                    2. **Filled tonal button states**
                    3. **Filled tonal button measurements**
                4. **Outlined button**
                    1. **Outlined button color**
                    2. **Outlined button states**
                    3. **Outlined button measurements**
                5. **Text button**
                    1. **Text button color**
                    2. **Text button states**
                    3. **Text button measurements**
            3. Guidelines
                1. **Usage**
                2. **Anatomy**
                    1. **Label text**
                    2. **Container**
                    3. **Icon (optional)**
                3. **Elevated buttons**
                4. **Filled buttons**
                5. **Filled tonal buttons**
                6. **Outlined buttons**
                7. **Text buttons**
                8. **Responsive layout**
                    1. **Buttons scale with their parent container**
            4. Accessibility
                1. **Use cases**
                2. **Interaction & style**
                3. **Keyboard navigation**
                4. **Labeling elements**
        3. FAB
            1. Overview
                1. Resources
                2. Differences
            2. Specs
                1. FAB
                    1. FAB color
                    2. **Additional color mappings**
                    3. **FAB states**
                    4. **FAB measurements**
                2. **Small FAB**
                    1. **Small FAB color**
                    2. **Additional color mappings**
                    3. **Small FAB states**
                    4. **Small FAB measurements**
                3. **Large FAB**
                    1. **Large FAB color**
                    2. **Additional color mappings**
                    3. **Large FAB states**
                    4. **Large FAB measurements**
            3. Guidelines
                1. Usage
                2. Anatomy
                    1. **Container**
                    2. Icon
                3. FAB
                4. Small FAB
                5. Large FAB
                6. Responsive layout
                    1. **Expanded window size**
                7. Behavior
                    1. Appearing
                    2. Screen transitions
                    3. Reappearance
                    4. Expanding
                    5. Actions
                    6. Scrolling
                    7. Moving across tabs
            4. Accessibility
                1. Interaction and style
                2. Focus
                3. Layout and position
                4. Keyboard navigation
                5. Labeling elements
        4. Extended FAB
            1. Overview
                1. Resources
                2. Differences
            2. Specs
                1. **Color**
                    1. **Additional color mappings**
                2. **States**
                3. Measurements
                4. Configurations
            3. Guidelines
                1. Usage
                2. Anatomy
                    1. Container
                    2. Icon
                    3. Label text
                3. Placement
                4. Responsive layout
                    1. Compact and medium
                    2. Expanded
                5. Behavior
                    1. Appearing
                    2. Expanding
                    3. **Transforming**
                    4. Scrolling
            4. Accessibility
                1. Use caases
                2. Interaction and style
                3. Initial focus
                4. Keyboard navigation
                5. Labeling elements
        5. Icon buttons
            1. Overview
            2. Specs
            3. Guidelines
            4. Accessiblity
        6. Segmented buttons
    5. Cards
    6. Carousel
    7. Checkbox
    8. Chips
    9. Date pickers
    10. Dialogs
    11. Divider
    12. Lists
    13. Menus
    14. Navigation
        1. Navigation bar
        2. Navigation drawer
        3. Navigation rail
    15. Progress indicators
    16. Search
    17. Sheets
        1. Bottom sheets
        2. Side sheets
    18. Sliders
    19. Snackbar
    20. Switch
    21. Tabs
    22. Text fields
    23. Time pickers
    24. Tooltips